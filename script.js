var encourage = (function() {
	var _this = {
		name: null,
		state: null,
		running: false,
		timeout: null,
		options: {
			minInterval: 1000 * 60 * 1,
			maxInterval: 1000 * 60 * 5,
			dev: location.host.indexOf('localhost') === 0
		},
		encouragements: [
			'You fucking rock, {name}!',
			'Dayumn {name}, that was awesome!',
			'You know who\'s rad? {name} is rad.',
			'I wish everyone in the world was like you {name}.',
			'You have beautiful eyes {name}.',
			'You have such an incredible singing voice {name}.',
			'How do you stay so awesome {name}?',
			'What\'s the secret behind your success, {name}?',
			'Keep up the good work {name}!',
			'I wish I got to spend more time with you {name}',
			'Have you heard the rumour that {name} is dating a supermodel?',
			'Oh my god I can\'t believe {name} is using my website!',
			'{name} is better looking than Brad Pitt and Mila Kunis combined.',
			'You\'re a very talented individual {name}.',
			'Go {name}! You can do it! I believe in you!',
			'Hey {name}, now\'s a good time to quietly give yourself a thumbs up.'
		],
		init: function() {
			_this.refresh();
			_this.bindEvents();

			if (sessionStorage['autorun'] == '1') {
				sessionStorage['autorun'] = '0';
				if (_this.state == 'go') {
					_this.run();
				}
			}

			window.addEventListener('beforeunload', function(e) {
				if (_this.running) {
					var confirmationMessage = 'Closing this window will prevent my encouragements from showing up!';

					(e || window.event).returnValue = confirmationMessage; //Gecko + IE
					return confirmationMessage; // Webkit & Blink
				}
			});
		},
		refresh: function() {
			_this.name = _this._getName();
			_this.state = _this._getState();
			document.body.setAttribute('data-state', _this.state);
			document.body.setAttribute('data-running', _this.running ? 1 : 0);
			document.body.setAttribute('data-supported', Notify.isSupported() ? 1 : 0);
			document.querySelector('section.go [data-field=name]').innerText = _this.name;

			if (_this.state == 'go')
				document.title = _this.name + ' is the best';
			else
				document.title = 'Who Is The Best?';
		},
		bindEvents: function() {
			// Name form
			document.querySelector('section.enterName form').addEventListener('submit', function(e) {
				e.preventDefault();
				var name = document.getElementById('name').value;
				if (!name)
					return;

				var callback = function() {
					sessionStorage['autorun'] = '1';
					if (_this.options.dev)
						location.href = '/index.html?name=' + encodeURIComponent(name).replace('%20', '+');
					else
						location.href = '/' + encodeURIComponent(name).replace('%20', '+');
				}

				if (Notify.needsPermission() && Notify.isSupported())
					Notify.requestPermission(callback, function() {
						alert('Notifications are blocked, how am I meant to encourage you?!');
					});
				else
					callback();
			});

			// Start/Stop Button
			document.querySelector('section.go form').addEventListener('submit', function(e) {
				e.preventDefault();
				if (_this.running)
					_this.stop();
				else
					_this.run();
			});
		},
		run: function() {
			if (!Notify.isSupported())
				return;

			_this.running = true;
			_this.refresh();

			if (Notify.needsPermission()) {
				Notify.requestPermission(function() {
					_this.showNotification('Nice work enabling those notifications, {name}!', 'Now leave this tab open while you go about your business.', 10);
				}, function() {
					alert('Notifications are blocked, how am I meant to encourage you?!');
					_this.stop();
				});
			} else {
				_this.showNotification('Great job hitting that button, {name}!', 'Now leave this tab open while you go about your business.', 10);
			}
		},
		stop: function() {
			_this.running = false;
			_this.refresh();
			clearTimeout(_this.timeout);
		},
		showNotification: function(encouragement, body, timeout) {
			if (!encouragement) {
				encouragement = _this.encouragements[Math.floor(Math.random() * _this.encouragements.length)];
			}

			var text = encouragement.replace(/\{name\}/g, _this.name);
			var options = {
				timeout: timeout || 5,
				tag: 'whoisthebest',
				icon: '/notification-icon.png'
			}
			if (body)
				options.body = body

			new Notify(text, options).show();

			_this.track('notification', 'show', encouragement);

			// Show next
			var interval = Math.round(_this.options.minInterval + (Math.random() * (_this.options.maxInterval - _this.options.minInterval)));
			_this.timeout = setTimeout(_this.showNotification, interval);
			_this.options.dev && console.log(interval);
		},
		track: function(category, action, label) {
			ga('send', 'event', category, action, label);
		},
		_getName: function() {
			// Query String
			var result,
				results = /[\\?&]name=([^&#]*)/.exec(location.search);
			if (results && results.length >= 2)
				result = results[1];
			// Path name
			else
				result = location.pathname.substr(1).replace(/\/.*/, '');
				
			if (!result)
				return null;

			result = decodeURIComponent(result.replace(/\+/g, '%20'));


			return result;
		},
		_getState: function() {
			if (_this._getName() === null)
				return 'enterName';
			else
				return 'go';
		}
	};
	return _this;
})();


encourage.init();